class Ticket < ApplicationRecord
	validates :title, :description, presence: true
	validates :title, uniqueness: true, length: { minimum: 10 }
	validates :description, length: { in: 20..100 }
end
