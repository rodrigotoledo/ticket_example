# INSTRUCOES PARA UTILIZACAO DO PROJETO

Para que o projeto seja executado corrementa voce pode seguir os passos abaixo

## Detalhes para instalacao do projeto rails:

* Ter o **rvm** instalado em sua máquina https://rvm.io/

* Ter o **SQLite** instalado em sua máquina pois utilizaremos o mesmo como base de dados

* Baixar o projeto e entrar na pasta do mesmo

* Instalar o **bundler** com o comando

``gem install bundler --no-ri --no-rdoc``

* Instalar as bibliotecas necessárias com o comando

``bundle install``

* Criar os bancos de dados com o comando

``rails db:drop db:create db:migrate db:seed``

### Pronto tudo OK, agora falta ver se está tudo funcionando

* Instalar o **Postman** em seu navegador **Chrome**

* Importar os testes localizados no arquivo **test/tickets_react_native_e_rails.postman_collection.json**

* Executar sua aplicação no terminal com o comando

``rails s``

## MUITO IMPORTANTE, OS TESTES UTILIZADOS NO POSTMAN USAM TOKENS DE USUÁRIOS GERADOS EM MINHA MÁQUINA, PORTANTO PARA QUE OS PASSOS DAQUI PRA FRENTE FUNCIONEM CORRETAMENTE VOCÊS DEVERÃO TROCAR O TOKEN INFORMADO NO CABEÇALHO INFORMADO NO _**POSTMAN**_

* Rodar o teste de criacao de tickets alterando os titulos.

**PS: SE ALGUÉM SOUBER COMO FAZER UM SCRIPT AUTOMATIZADO NO POSTMAN PARA QUE O MESMO ALTERE CONTEÚDO DINAMICAMENTE E QUISER CONTRIBUIR IREI AGRADECER**

* Realizar testes usando **Postman** lembrando sempre de atualizar o token de autenticação.
