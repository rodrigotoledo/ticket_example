Rails.application.routes.draw do
  devise_for :users, only: [:sessions], controllers: {sessions: 'sessions'}
  resources :tickets
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'tickets#index'
end
